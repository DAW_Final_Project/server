FROM node:14.17.0-alpine
WORKDIR /app
COPY . .
EXPOSE 4323
RUN rm -Rf node_modules
RUN npm install
CMD ["/bin/sh",  "-c",  "node src/server.js"]
