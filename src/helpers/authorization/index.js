const { ObjectId } = require("mongodb");
const { tokenGenerator } = require("./tokenGenerator");
const db = require("../../mongodb/db")

const logator = async (authCode, store) => {
  let authorized = false;
  let role_code = "";
  let access_id = "";
  let user="";
  let token = tokenGenerator();
  try {
    if (authCode) {
      const encoded = authCode.substring(6);
      const decoded = Buffer.from(encoded, "base64").toString("ascii");
      const [username, password] = decoded.split(":");
      const response = await db.reader(
        "accesses",
        { username: username },
        { password: 1, role_id: 1, user_id: 1 }
      );
      if (response.length > 0) {
        if (response[0].password == password) {
          const response2 = await db.reader(
            "users",
            { _id: ObjectId(response[0].user_id) },
            { _id: 0, stores: 1, name:1, surnames:1 }
          ); 
          if (response2.length > 0) {
            for (let i = 0; i < response2[0].stores.length; i++) {
              if (response2[0].stores[i] == store) {
                authorized = true;
              }
            }
            user=`${response2[0].name} ${response2[0].surnames}`
          }
          const response3 = await db.reader(
            "roles",
            { _id: ObjectId(response[0].role_id) },
            { _id: 0, code: 1 }
          );
          const response4 = await db.updater(
            "accesses",
            { _id: ObjectId(response[0]._id) },
            { token: token }
          );
          role_code = response3[0].code;
          access_id = response[0]._id;
        }
      }
    }
    return {
      authorized: authorized,
      token: token,
      role: role_code,
      id: access_id,
      user:user,
    };
  } catch (error) {
    console.log(error);
    return {
      authorized: false,
      token:"",
      role: "",
      id: "",
      user:"",
    };
  }
};

const authorizator = async (authCode, rols) => {
  let authorized = false;
  try {
    if (authCode != null) {
      const response = await db.reader(
        "accesses",
        { token: authCode },
        { _id: 0, role_id: 1 }
      );
      if (response.length > 0) {
        const response2 = await db.reader(
          "roles",
          { _id: ObjectId(response[0].role_id) },
          { _id: 0, code: 1 }
        );
        if (response2.length > 0) {
          if (rols.includes(response2[0].code)) {
            authorized = true;
          }
        }
      }
    }
  } catch (error) {
    console.log(error);
  } finally {
    if (authorized) {
      // console.log("Access Granted");
      return true;
    } else {
      // console.log("Forbiden");
      return false;
    }
  }
};

module.exports = { authorizator, logator };
