// Requireds
const dotenv = require("dotenv").config();
const { schemas } = require("./schemas/schemas");
const mongoose = require("mongoose");
mongoose.Promise = global.Promise;
let collections = [
  "roles",
  "stores",
  "users",
  "accesses",
  "clients",
  "productscategories",
  "productstrademarks",
  "products",
  "materialscategories",
  "materials",
  "status",
  "operations",
  "services",
];
let models = {};

exports.connector = async () => {
  try {
    console.log("\x1b[33m%s\x1b[0m", `Conecting Database`);
    const uri = `mongodb://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`;
    await mongoose.connect(uri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    console.log("\x1b[32m%s\x1b[0m", `\tConection Successful\n`);
  } catch (error) {
    console.log(error);
  }
};

exports.disconnector = async () => {
  try {
    console.log("\x1b[33m%s\x1b[0m", `Disconnecting Database`);
    await mongoose.connection.close();
    console.log("\x1b[32m%s\x1b[0m", `\tDisconection Successful\n`);
  } catch (error) {
    console.log(error);
  }
};

exports.generator = async () => {
  try {
    console.log("\x1b[33m%s\x1b[0m", `Generating Models`);
    // Schemas and Models
    for (let i = 0; i < collections.length; i++) {
      let Schema = new mongoose.Schema(schemas[collections[i]]);
      models[collections[i]] = mongoose.model(collections[i], Schema);
    }
    console.log("\x1b[32m%s\x1b[0m", `\tAll Models Generated Successfuly\n`);
  } catch (error) {
    console.log(error);
  }
};

exports.reader = async (
  collectionName,
  filter = {},
  project = {},
  sort = {},
  skip = 0,
  limit = 0
) => {
  try {
    let result = await models[collectionName]
      .find(filter, project, { skip: skip, limit: limit })
      .sort(sort)
      .exec();
    return result;
  } catch (error) {
    console.log(error);
  }
};

exports.counter = async (collectionName, filter = {}) => {
  try {
    let result = await models[collectionName].countDocuments(filter);
    return result;
  } catch (error) {
    console.log(error);
  }
};

exports.creator = async (collectionName, data = {}) => {
  try {
    let result = await models[collectionName].create(data);
    return result;
  } catch (error) {
    console.log(error);
  }
};

exports.updater = async (collectionName, filter = {}, data = {}) => {
  try {
    let result = await models[collectionName].findOneAndUpdate(filter, data);
    return result;
  } catch (error) {
    console.log(error);
  }
};

exports.deleter = async (collectionName, filter = {}) => {
  try {
    let result = await models[collectionName].findOneAndRemove(filter);
    return result;
  } catch (error) {
    console.log(error);
  }
};