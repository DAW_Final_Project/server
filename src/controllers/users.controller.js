const { ObjectId } = require("mongodb");
const { authorizator } = require("../helpers/authorization");
const db = require("../mongodb/db")

/* const getUsersList = async (req, res) => {
  try {
    if (req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization);
      if (authorization) {
        return res.status(200).send({ message: "Access Granted" });
      } else {
        return res.status(403).send({ message: "Forbiden" });
      }
    } else {
      return res.status(401).send({ message: "Bad Request" });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).send({ message: error });
  }
}; */

const getUsersList = async (req, res) => {
  console.log('\x1b[35m%s\x1b[0m',`(${(new Date()).toLocaleString('es-ES')}) => GET - Users List`)
  try {
    let sort = {};
    if (req.query.sort) {
      let order = req.query.order && req.query.order == "desc" ? -1 : 1;
      sort[req.query.sort] = order;
    }
    let limit = req.query.limit ? parseInt(req.query.limit) : 0;
    let skip = req.query.skip ? parseInt(req.query.skip) : 0;
    const list = await db.reader(
      "users",
      {},
      { name: 1, surnames: 1, document: 1 },
      sort,
      skip,
      limit
    );
    const total = await db.counter("users");
    const data = {
      list: list,
      total: total,
    };
    console.log('\x1b[38;5;28m\x1b[1m%s\x1b[0m',"\t\tResult: 200 -> Users List Sent")
    return res.status(200).json(data);
  } catch (error) {
    console.log('\x1b[31m\x1b[1m%s\x1b[0m',"\t\tResult: 500 -> Server Error")
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const getUserData = async (req, res) => {
  console.log('\x1b[35m%s\x1b[0m',`(${(new Date()).toLocaleString('es-ES')}) => GET - User Data`)
  try {
    const data = await db.reader("users", {
      _id: ObjectId(req.params.user_id),
    });
    console.log('\x1b[38;5;28m\x1b[1m%s\x1b[0m',"\t\tResult: 200 -> User Data Sent")
    return res.status(200).json(data);
  } catch (error) {
    console.log('\x1b[31m\x1b[1m%s\x1b[0m',"\t\tResult: 500 -> Server Error")
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const addUser = async (req, res) => {
  console.log('\x1b[35m%s\x1b[0m',`(${(new Date()).toLocaleString('es-ES')}) => POST - Add User`)
  try {
    console.log("REQUEST", req.body);
    let body = req.body;
    const data = await db.creator("users", body);
    if (data) {
      console.log('\x1b[38;5;28m\x1b[1m%s\x1b[0m',"\t\tResult: 200 -> New User Added")
      return res.status(200);
    } else {      
      console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 405 -> User Not Added")
      return res.status(405);
    }
  } catch (error) {
    console.log('\x1b[31m\x1b[1m%s\x1b[0m',"\t\tResult: 500 -> Server Error")
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const modifyUser = async (req, res) => {
  console.log('\x1b[35m%s\x1b[0m',`(${(new Date()).toLocaleString('es-ES')}) => PUT - Modify User Data`)
  try {
    let body = req.body;
    const data = await db.updater(
      "users",
      {
        _id: ObjectId(req.params.user_id),
      },
      body,
      false
    );
    if (data) {
      console.log('\x1b[38;5;28m\x1b[1m%s\x1b[0m',"\t\tResult: 200 -> User Data Modified")
      return res.status(200);
    } else {      
      console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 404 -> User Not Found")
      return res.status(404);
    }
  } catch (error) {
    console.log('\x1b[31m\x1b[1m%s\x1b[0m',"\t\tResult: 500 -> Server Error")
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const deleteUser = async (req, res) => {
  console.log('\x1b[35m%s\x1b[0m',`(${(new Date()).toLocaleString('es-ES')}) => DELETE - Delete User`)
  try {
    console.log("user_id", req.params.user_id);
    const data = await db.deleter("users", {
      _id: ObjectId(req.params.user_id),
    });
    if (data) {
      console.log('\x1b[38;5;28m\x1b[1m%s\x1b[0m',"\t\tResult: 200 -> User Deleted")
      return res.status(200);
    } else {      
      console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 404 -> User Not Found")
      return res.status(404);
    }
  } catch (error) {
    console.log('\x1b[31m\x1b[1m%s\x1b[0m',"\t\tResult: 500 -> Server Error")
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

module.exports = {
  getUsersList,
  getUserData,
  addUser,
  modifyUser,
  deleteUser,
};
