const { logator } = require("../helpers/authorization");

const login = async (req, res) => {
  console.log('\x1b[35m%s\x1b[0m',`(${(new Date()).toLocaleString('es-ES')}) => GET - Login`)
  try {
    if (req.headers.authorization) {
      let authorization = await logator(req.headers.authorization, req.query.store);
      if (authorization.authorized) {
        delete authorization.authorized
        console.log('\x1b[38;5;28m\x1b[1m%s\x1b[0m',"\t\tResult: 200 -> Access Granted")
        return res.status(200).json(authorization);
      } else {
        console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
        return res.status(403).send({ message: "Forbiden" });
      }
    } else {
      console.log('\x1b[31m\x1b[1m%s\x1b[0m',"\t\tResult: 401 -> Bad Request")
      return res.status(401).send({ message: "Bad Request" });
    }
  } catch (error) {
    console.log('\x1b[31m\x1b[1m%s\x1b[0m',"\t\tResult: 500 -> Server Error")
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

module.exports = {
  login,
};
