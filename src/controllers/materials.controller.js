const { ObjectId } = require("mongodb");
const { authorizator } = require("../helpers/authorization");
const db = require("../mongodb/db")

const getMaterialsList = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => GET - Materials List`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2])
      if (authorization) {
        let sort = {};
        if (req.query.sort) {
          let order = req.query.order && req.query.order == "desc" ? -1 : 1;
          sort[req.query.sort] = order;
        }
        let limit = req.query.limit ? parseInt(req.query.limit) : 0;
        let skip = req.query.skip ? parseInt(req.query.skip) : 0;
        const list = await db.reader(
          "materials",
          {},
          { name: 1, surnames: 1, document: 1 },
          sort,
          skip,
          limit
        );
        const total = await db.counter("materials");
        const data = {
          list: list,
          total: total,
        };
        console.log(
          "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
          "\t\tResult: 200 -> Materials List Sent"
        );
        return res.status(200).json(data);
      } else {
        console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
        return res.status(403).send({ message: "Forbiden" });
      }
    } else {
      console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
      return res.status(403).send({ message: "Forbiden" });
    }
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const getMaterialData = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => GET - Material Data`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2])
      if (authorization) {
    const data = await db.reader("materials", {
      _id: ObjectId(req.params.material_id),
    });
    if (data) {
      console.log(
        "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
        "\t\tResult: 200 -> Material Data Sent"
      );
      return res.status(200).json(data);
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 404 -> Material Not Found"
      );
      return res.status(404).send({ message: "Material Not Found" });;
    }
  } else {
    console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
    return res.status(403).send({ message: "Forbiden" });
  }
} else {
  console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
  return res.status(403).send({ message: "Forbiden" });
}
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const addMaterial = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => POST - Add Material`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2])
      if (authorization) {
    console.log("REQUEST BODY", req.body);
    let body = req.body;
    const data = await db.creator("materials", body);
    if (data) {
      console.log(
        "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
        "\t\tResult: 200 -> New Material Added"
      );
      return res.status(200).send({ message: "New Material Added" });
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 405 -> Material Not Added"
      );
      return res.status(405).send({ message: "Material Not Added" });
    }
  } else {
    console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
    return res.status(403).send({ message: "Forbiden" });
  }
} else {
  console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
  return res.status(403).send({ message: "Forbiden" });
}
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const modifyMaterial = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => PUT - Modify Material Data`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2])
      if (authorization) {
    let body = req.body;
    const data = await db.updater(
      "materials",
      {
        _id: ObjectId(req.params.material_id),
      },
      body,
      false
    );
    if (data) {
      console.log(
        "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
        "\t\tResult: 200 -> Material Data Modified"
      );
      return res.status(200);
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 404 -> Material Not Found"
      );
      return res.status(404).send({ message: "Material Not Found" });;
    }
  } else {
    console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
    return res.status(403).send({ message: "Forbiden" });
  }
} else {
  console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
  return res.status(403).send({ message: "Forbiden" });
}
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const deleteMaterial = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => DELETE - Delete Material`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2])
      if (authorization) {
    console.log("material_id", req.params.material_id);
    const data = await db.deleter("materials", {
      _id: ObjectId(req.params.material_id),
    });
    if (data) {
      console.log(
        "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
        "\t\tResult: 200 -> Material Deleted"
      );
      return res.status(200);
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 404 -> Material Not Found"
      );
      return res.status(404).send({ message: "Material Not Found" });
    }
  } else {
    console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
    return res.status(403).send({ message: "Forbiden" });
  }
} else {
  console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
  return res.status(403).send({ message: "Forbiden" });
}
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

module.exports = {
  getMaterialsList,
  getMaterialData,
  addMaterial,
  modifyMaterial,
  deleteMaterial,
};
