const { ObjectId } = require("mongodb");
const { authorizator } = require("../helpers/authorization");
const db = require("../mongodb/db")

const getTrademarksList = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => GET - Trademark List`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2])
      if (authorization) {
        let sort = {};
        if (req.query.sort) {
          let order = req.query.order && req.query.order == "desc" ? -1 : 1;
          sort[req.query.sort] = order;
        }
        let limit = req.query.limit ? parseInt(req.query.limit) : 0;
        let skip = req.query.skip ? parseInt(req.query.skip) : 0;
        const list = await db.reader(
          "productstrademarks",
          {},
          { trademark: 1 },
          sort,
          skip,
          limit
        );
        const total = await db.counter("productstrademarks");
        const data = {
          list: list,
          total: total,
        };
        console.log(
          "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
          "\t\tResult: 200 -> Trademark List Sent"
        );
        return res.status(200).json(data);
      } else {
        console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
        return res.status(403).send({ message: "Forbiden" });
      }
    } else {
      console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
      return res.status(403).send({ message: "Forbiden" });
    }
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const getTrademarkData = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => GET - Trademark Data`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2])
      if (authorization) {
    const data = await db.reader("productstrademarks", {
      _id: ObjectId(req.params.trademark_id),
    });
    if (data) {
      console.log(
        "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
        "\t\tResult: 200 -> Trademark Data Sent"
      );
      return res.status(200).json(data[0]);
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 404 -> Trademark Not Found"
      );
      return res.status(404).send({ message: "Trademark Not Found" });;
    }
  } else {
    console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
    return res.status(403).send({ message: "Forbiden" });
  }
} else {
  console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
  return res.status(403).send({ message: "Forbiden" });
}
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const addTrademark = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => POST - Add Trademark`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2])
      if (authorization) {
    console.log("REQUEST BODY", req.body);
    let body = req.body;
    const data = await db.creator("productstrademarks", body);
    if (data) {
      console.log(
        "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
        "\t\tResult: 200 -> New Trademark Added"
      );
      return res.status(200).send({ message: "New Trademark Added" });
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 405 -> Trademark Not Added"
      );
      return res.status(405).send({ message: "Trademark Not Added" });
    }
  } else {
    console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
    return res.status(403).send({ message: "Forbiden" });
  }
} else {
  console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
  return res.status(403).send({ message: "Forbiden" });
}
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const modifyTrademark = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => PUT - Modify Trademark Data`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2])
      if (authorization) {
    let body = req.body;
    const data = await db.updater(
      "productstrademarks",
      {
        _id: ObjectId(req.params.trademark_id),
      },
      body,
      false
    );
    if (data) {
      console.log(
        "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
        "\t\tResult: 200 -> Trademark Data Modified"
      );
      return res.status(200);
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 404 -> Trademark Not Found"
      );
      return res.status(404).send({ message: "Trademark Not Found" });;
    }
  } else {
    console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
    return res.status(403).send({ message: "Forbiden" });
  }
} else {
  console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
  return res.status(403).send({ message: "Forbiden" });
}
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const deleteTrademark = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => DELETE - Delete Trademark`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2])
      if (authorization) {
    console.log("trademark_id", req.params.trademark_id);
    const data = await db.deleter("productstrademarks", {
      _id: ObjectId(req.params.trademark_id),
    });
    if (data) {
      console.log(
        "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
        "\t\tResult: 200 -> Trademark Deleted"
      );
      return res.status(200);
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 404 -> Trademark Not Found"
      );
      return res.status(404).send({ message: "Trademark Not Found" });
    }
  } else {
    console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
    return res.status(403).send({ message: "Forbiden" });
  }
} else {
  console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
  return res.status(403).send({ message: "Forbiden" });
}
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

module.exports = {
  getTrademarksList,
  getTrademarkData,
  addTrademark,
  modifyTrademark,
  deleteTrademark,
};
