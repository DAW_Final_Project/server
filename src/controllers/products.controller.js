const { ObjectId } = require("mongodb");
const { authorizator } = require("../helpers/authorization");
const db = require("../mongodb/db")

const getProductsList = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => GET - Product List`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2])
      if (authorization) {
        let sort = {};
        if (req.query.sort) {
          let order = req.query.order && req.query.order == "desc" ? -1 : 1;
          sort[req.query.sort] = order;
        }
        let limit = req.query.limit ? parseInt(req.query.limit) : 0;
        let skip = req.query.skip ? parseInt(req.query.skip) : 0;
        const list = await db.reader(
          "products",
          {trademark_id: req.params.trademark_id,
          category_id: req.params.category_id},
          {},
          sort,
          skip,
          limit
        );
        const total = await db.counter("products");
        const data = {
          list: list,
          total: total,
        };
        console.log(
          "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
          "\t\tResult: 200 -> Product List Sent"
        );
        return res.status(200).json(data);
      } else {
        console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
        return res.status(403).send({ message: "Forbiden" });
      }
    } else {
      console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
      return res.status(403).send({ message: "Forbiden" });
    }
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const getProductData = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => GET - Product Data`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2])
      if (authorization) {
    const data = await db.reader("products", {
      _id: ObjectId(req.params.product_id),
    });
    if (data) {
      console.log(
        "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
        "\t\tResult: 200 -> Product Data Sent"
      );
      return res.status(200).json(data[0]);
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 404 -> Product Not Found"
      );
      return res.status(404).send({ message: "Product Not Found" });;
    }
  } else {
    console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
    return res.status(403).send({ message: "Forbiden" });
  }
} else {
  console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
  return res.status(403).send({ message: "Forbiden" });
}
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const addProduct = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => POST - Add Product`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2])
      if (authorization) {
    console.log("REQUEST BODY", req.body);
    let body = req.body;
    const data = await db.creator("products", body);
    if (data) {
      console.log(
        "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
        "\t\tResult: 200 -> New Product Added"
      );
      return res.status(200).send({ message: "New Product Added" });
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 405 -> Product Not Added"
      );
      return res.status(405).send({ message: "Product Not Added" });
    }
  } else {
    console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
    return res.status(403).send({ message: "Forbiden" });
  }
} else {
  console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
  return res.status(403).send({ message: "Forbiden" });
}
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const modifyProduct = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => PUT - Modify Product Data`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2])
      if (authorization) {
    let body = req.body;
    const data = await db.updater(
      "products",
      {
        _id: ObjectId(req.params.product_id),
      },
      body,
      false
    );
    if (data) {
      console.log(
        "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
        "\t\tResult: 200 -> Product Data Modified"
      );
      return res.status(200);
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 404 -> Product Not Found"
      );
      return res.status(404).send({ message: "Product Not Found" });;
    }
  } else {
    console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
    return res.status(403).send({ message: "Forbiden" });
  }
} else {
  console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
  return res.status(403).send({ message: "Forbiden" });
}
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const deleteProduct = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => DELETE - Delete Product`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2])
      if (authorization) {
    console.log("product_id", req.params.product_id);
    const data = await db.deleter("products", {
      _id: ObjectId(req.params.product_id),
    });
    if (data) {
      console.log(
        "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
        "\t\tResult: 200 -> Product Deleted"
      );
      return res.status(200);
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 404 -> Product Not Found"
      );
      return res.status(404).send({ message: "Product Not Found" });
    }
  } else {
    console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
    return res.status(403).send({ message: "Forbiden" });
  }
} else {
  console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
  return res.status(403).send({ message: "Forbiden" });
}
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

module.exports = {
  getProductsList,
  getProductData,
  addProduct,
  modifyProduct,
  deleteProduct,
};
