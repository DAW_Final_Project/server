const { ObjectId } = require("mongodb");
const { authorizator } = require("../helpers/authorization");
const db = require("../mongodb/db")

const getClientsList = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => GET - Client List`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2]);
      if (authorization) {
        let sort = {};
        if (req.query.sort) {
          let order = req.query.order && req.query.order == "desc" ? -1 : 1;
          sort[req.query.sort] = order;
        }
        let limit = req.query.limit ? parseInt(req.query.limit) : 0;
        let skip = req.query.skip ? parseInt(req.query.skip) : 0;
        const list = await db.reader(
          "clients",
          {},
          { created_at: 1, name: 1, surnames: 1, document: 1 },
          sort,
          skip,
          limit
        );
        const total = await db.counter("clients");
        const data = {
          list: list,
          total: total,
        };
        console.log(
          "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
          "\t\tResult: 200 -> Client List Sent"
        );
        return res.status(200).json(data);
      } else {
        console.log(
          "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
          "\t\tResult: 403 -> Access Denied"
        );
        return res.status(403).send({ message: "Forbiden" });
      }
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 403 -> Access Denied"
      );
      return res.status(403).send({ message: "Forbiden" });
    }
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const getClientData = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => GET - Client Data`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2]);
      if (authorization) {
        const data = await db.reader("clients", {
          _id: ObjectId(req.params.client_id),
        });
        if (data) {
          console.log(
            "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
            "\t\tResult: 200 -> Client Data Sent"
          );
          return res.status(200).json(data);
        } else {
          console.log(
            "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
            "\t\tResult: 404 -> Client Not Found"
          );
          return res.status(404).send({ message: "Client Not Found" });
        }
      } else {
        console.log(
          "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
          "\t\tResult: 403 -> Access Denied"
        );
        return res.status(403).send({ message: "Forbiden" });
      }
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 403 -> Access Denied"
      );
      return res.status(403).send({ message: "Forbiden" });
    }
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const addClient = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => POST - Add Client`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2]);
      if (authorization) {
        console.log("REQUEST BODY", req.body);
        let body = req.body;
        body.created_at=Date.now()
        const data = await db.creator("clients", body);
        if (data) {
          console.log(
            "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
            "\t\tResult: 200 -> New Client Added"
          );
          return res.status(200).send({ message: "New Client Added" });
        } else {
          console.log(
            "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
            "\t\tResult: 405 -> Client Not Added"
          );
          return res.status(405).send({ message: "Client Not Added" });
        }
      } else {
        console.log(
          "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
          "\t\tResult: 403 -> Access Denied"
        );
        return res.status(403).send({ message: "Forbiden" });
      }
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 403 -> Access Denied"
      );
      return res.status(403).send({ message: "Forbiden" });
    }
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const modifyClient = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => PUT - Modify Client Data`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2]);
      if (authorization) {
        let body = req.body;
        const data = await db.updater(
          "clients",
          {
            _id: ObjectId(req.params.client_id),
          },
          body
        );
        if (data) {
          console.log(
            "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
            "\t\tResult: 200 -> Client Data Modified"
          );
          return res.status(200);
        } else {
          console.log(
            "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
            "\t\tResult: 404 -> Client Not Found"
          );
          return res.status(404).send({ message: "Client Not Found" });
        }
      } else {
        console.log(
          "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
          "\t\tResult: 403 -> Access Denied"
        );
        return res.status(403).send({ message: "Forbiden" });
      }
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 403 -> Access Denied"
      );
      return res.status(403).send({ message: "Forbiden" });
    }
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const deleteClient = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => DELETE - Delete Client`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2]);
      if (authorization) {
        console.log("client_id", req.params.client_id);
        const data = await db.deleter("clients", {
          _id: ObjectId(req.params.client_id),
        });
        if (data) {
          console.log(
            "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
            "\t\tResult: 200 -> Client Deleted"
          );
          return res.status(200).send({ message: "Client Deleted" });
        } else {
          console.log(
            "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
            "\t\tResult: 404 -> Client Not Found"
          );
          return res.status(404).send({ message: "Client Not Found" });
        }
      } else {
        console.log(
          "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
          "\t\tResult: 403 -> Access Denied"
        );
        return res.status(403).send({ message: "Forbiden" });
      }
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 403 -> Access Denied"
      );
      return res.status(403).send({ message: "Forbiden" });
    }
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

module.exports = {
  getClientsList,
  getClientData,
  addClient,
  modifyClient,
  deleteClient,
};
