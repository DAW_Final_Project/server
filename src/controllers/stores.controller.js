const { ObjectId } = require("mongodb");
const { authorizator } = require("../helpers/authorization");
const db = require("../mongodb/db")



const getStoresListForLogin = async (req, res) => {
  console.log('\x1b[35m%s\x1b[0m',`(${(new Date()).toLocaleString('es-ES')}) => GET - Stores List For Login`)
  try {
    let sort = {};
    if (req.query.sort) {
      let order = req.query.order && req.query.order == "desc" ? -1 : 1;
      sort[req.query.sort] = order;
    }
    let limit = req.query.limit ? parseInt(req.query.limit) : 0;
    let skip = req.query.skip ? parseInt(req.query.skip) : 0;
    const list = await db.reader(
      "stores",
      {},
      { name: 1 },
      sort,
      skip,
      limit
    );
    console.log('\x1b[38;5;28m\x1b[1m%s\x1b[0m',"\t\tResult: 200 -> Stores List Sent")
    return res.status(200).json(list);
  } catch (error) {
    console.log('\x1b[31m\x1b[1m%s\x1b[0m',"\t\tResult: 500 -> Server Error")
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const getStoresList = async (req, res) => {
  console.log('\x1b[35m%s\x1b[0m',`(${(new Date()).toLocaleString('es-ES')}) => GET - Stores List`)
  try {
    let sort = {};
    if (req.query.sort) {
      let order = req.query.order && req.query.order == "desc" ? -1 : 1;
      sort[req.query.sort] = order;
    }
    let limit = req.query.limit ? parseInt(req.query.limit) : 0;
    let skip = req.query.skip ? parseInt(req.query.skip) : 0;
    const list = await db.reader(
      "stores",
      {},
      { name: 1, surnames: 1, document: 1 },
      sort,
      skip,
      limit
    );
    const total = await db.counter("stores");
    const data = {
      list: list,
      total: total,
    };
    console.log('\x1b[38;5;28m\x1b[1m%s\x1b[0m',"\t\tResult: 200 -> Stores List Sent")
    return res.status(200).json(data);
  } catch (error) {
    console.log('\x1b[31m\x1b[1m%s\x1b[0m',"\t\tResult: 500 -> Server Error")
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const getStoreData = async (req, res) => {
  console.log('\x1b[35m%s\x1b[0m',`(${(new Date()).toLocaleString('es-ES')}) => GET - Store Data`)
  try {
    const data = await db.reader("stores", {
      _id: ObjectId(req.params.store_id),
    });
    console.log('\x1b[38;5;28m\x1b[1m%s\x1b[0m',"\t\tResult: 200 -> Store Data Sent")
    return res.status(200).json(data[0]);
  } catch (error) {
    console.log('\x1b[31m\x1b[1m%s\x1b[0m',"\t\tResult: 500 -> Server Error")
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const addStore = async (req, res) => {
  console.log('\x1b[35m%s\x1b[0m',`(${(new Date()).toLocaleString('es-ES')}) => POST - Add Store`)
  try {
    console.log("REQUEST", req.body);
    let body = req.body;
    const data = await db.creator("stores", body);
    if (data) {
      console.log('\x1b[38;5;28m\x1b[1m%s\x1b[0m',"\t\tResult: 200 -> New Store Added")
      return res.status(200);
    } else {      
      console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 405 -> Store Not Added")
      return res.status(405);
    }
  } catch (error) {
    console.log('\x1b[31m\x1b[1m%s\x1b[0m',"\t\tResult: 500 -> Server Error")
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const modifyStore = async (req, res) => {
  console.log('\x1b[35m%s\x1b[0m',`(${(new Date()).toLocaleString('es-ES')}) => PUT - Modify Store Data`)
  try {
    let body = req.body;
    const data = await db.updater(
      "stores",
      {
        _id: ObjectId(req.params.store_id),
      },
      body,
      false
    );
    if (data) {
      console.log('\x1b[38;5;28m\x1b[1m%s\x1b[0m',"\t\tResult: 200 -> Store Data Modified")
      return res.status(200);
    } else {      
      console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 404 -> Store Not Found")
      return res.status(404);
    }
  } catch (error) {
    console.log('\x1b[31m\x1b[1m%s\x1b[0m',"\t\tResult: 500 -> Server Error")
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const deleteStore = async (req, res) => {
  console.log('\x1b[35m%s\x1b[0m',`(${(new Date()).toLocaleString('es-ES')}) => DELETE - Delete Store`)
  try {
    console.log("store_id", req.params.store_id);
    const data = await db.deleter("stores", {
      _id: ObjectId(req.params.store_id),
    });
    if (data) {
      console.log('\x1b[38;5;28m\x1b[1m%s\x1b[0m',"\t\tResult: 200 -> Store Deleted")
      return res.status(200);
    } else {      
      console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 404 -> Store Not Found")
      return res.status(404);
    }
  } catch (error) {
    console.log('\x1b[31m\x1b[1m%s\x1b[0m',"\t\tResult: 500 -> Server Error")
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

module.exports = {
  getStoresListForLogin,
  getStoresList,
  getStoreData,
  addStore,
  modifyStore,
  deleteStore,
};
