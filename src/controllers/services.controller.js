const { ObjectId } = require("mongodb");
const { authorizator } = require("../helpers/authorization");
const db = require("../mongodb/db");

const getServicesListByClient = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => GET - Services List`
  );
  try {
    let sort = {};
    if (req.query.sort) {
      let order = req.query.order && req.query.order == "desc" ? -1 : 1;
      sort[req.query.sort] = order;
    }
    let limit = req.query.limit ? parseInt(req.query.limit) : 0;
    let skip = req.query.skip ? parseInt(req.query.skip) : 0;
    let list = await db.reader(
      "services",
      { client_id: req.params.client_id },
      { description: 1, operations: 1, created_at: 1 },
      sort,
      skip,
      limit
    );
    let statusList = await db.reader("status", {}, { code: 1 });
    let statusObject = {};
    statusList.forEach((element) => {
      statusObject[element.code] = element._id.toString();
    });
    let list2 = [];
    for (let i = 0; i < list.length; i++) {
      let temp = {};
      temp._id = list[i]._id;
      temp.description = list[i].description;
      temp.created_at = list[i].created_at;
      let operationsStatus = [];
      for (let j = 0; j < list[i].operations.length; j++) {
        if (list[i].operations[j].status_id == statusObject["1"]) {
          operationsStatus.push("1");
        }
        if (list[i].operations[j].status_id == statusObject["2"]) {
          operationsStatus.push("2");
        }
        if (list[i].operations[j].status_id == statusObject["3"]) {
          operationsStatus.push("3");
        }
      }
      if (operationsStatus.includes("2")) {
        temp.status = 2;
      } else if (
        operationsStatus.includes("1") &&
        operationsStatus.includes("3")
      ) {
        temp.status = 2;
      } else if (operationsStatus.includes("3")) {
        temp.status = 3;
      } else {
        temp.status = 1;
      }
      temp.operations=list[i].operations.length
      list2.push(temp);
    }

    const total = await db.counter("services", {
      client_id: req.params.client_id,
    });
    const data = {
      list: list2,
      total: total,
    };
    console.log(
      "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
      "\t\tResult: 200 -> Services List Sent"
    );
    return res.status(200).json(data);
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const getServiceData = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => GET - Service Data`
  );
  try {
    console.log("service_id", req.params.service_id);
    let service = await db.reader("services", {
      _id: ObjectId(req.params.service_id),
    });
    let statusList = await db.reader("status", {}, { code: 1 });
    let statusObject = {};
    statusList.forEach((element) => {
      statusObject[element.code] = element._id.toString();
    });

    let service2 = [];
    if (service[0]) {
      let operations2 = [];
      for (let i = 0; i < service[0].operations.length; i++) {
        let temp = JSON.parse(JSON.stringify(service[0].operations[i]));
        let operation = await db.reader(
          "operations",
          {
            _id: ObjectId(service[0].operations[i].operation_id),
          },
          { alias: 1, _id: 0 }
        );
        temp.alias = operation[0].alias;
        if (service[0].operations[i].status_id == statusObject["1"]) {
          temp.status = 1;
        }
        if (service[0].operations[i].status_id == statusObject["2"]) {
          temp.status = 2;
        }
        if (service[0].operations[i].status_id == statusObject["3"]) {
          temp.status = 3;
        }
        operations2.push(temp);
      }
      let temp2 = JSON.parse(JSON.stringify(service[0]));
      temp2.operations = operations2;
      service2.push(temp2);
    }
    console.log(
      "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
      "\t\tResult: 200 -> Client Data Sent"
    );
    return res.status(200).json(service2[0]);
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const addService = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => POST - Add Service`
  );
  try {
    let body = req.body;
    let status = await db.reader("status", {
      status: "received",
    });
    for (let i = 0; i < body.operations.length; i++) {
      body.operations[i].status_id = status[0]._id.toString();
      body.operations[i].usuario_id = null;
    }
    body.created_at = Date.parse(new Date());
    const data = await db.creator("services", body);
    if (data) {
      console.log(
        "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
        "\t\tResult: 200 -> New Service Added"
      );
      return res.status(200).send({ message: "New Service Added" });
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 405 -> Service Not Added"
      );
      return res.status(405).send({ message: "Service Not Added" });
    }
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const modifyService = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => PUT - Modify Service Data`
  );
  try {
    let body = req.body;
    const data = await db.updater(
      "services",
      {
        _id: ObjectId(req.params.service_id),
      },
      body,
      false
    );
    if (data) {
      console.log(
        "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
        "\t\tResult: 200 -> Service Data Modified"
      );
      return res.status(200);
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 404 -> Service Not Found"
      );
      return res.status(404);
    }
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const deleteService = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => DELETE - Delete Service`
  );
  try {
    console.log("service_id", req.params.service_id);
    const data = await db.deleter("services", {
      _id: ObjectId(req.params.service_id),
    });
    if (data) {
      console.log(
        "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
        "\t\tResult: 200 -> Service Deleted"
      );
      return res.status(200);
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 404 -> Service Not Found"
      );
      return res.status(404);
    }
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

module.exports = {
  getServicesListByClient,
  getServiceData,
  addService,
  modifyService,
  deleteService,
};
