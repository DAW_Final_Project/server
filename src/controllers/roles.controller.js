const { ObjectId } = require("mongodb");
const { authorizator } = require("../helpers/authorization");
const db = require("../mongodb/db")

const getRolesList = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => GET - Role List`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2])
      if (authorization) {
        let sort = {};
        if (req.query.sort) {
          let order = req.query.order && req.query.order == "desc" ? -1 : 1;
          sort[req.query.sort] = order;
        }
        let limit = req.query.limit ? parseInt(req.query.limit) : 0;
        let skip = req.query.skip ? parseInt(req.query.skip) : 0;
        const list = await db.reader(
          "roles",
          {},
          { name: 1, surnames: 1, document: 1 },
          sort,
          skip,
          limit
        );
        const total = await db.counter("roles");
        const data = {
          list: list,
          total: total,
        };
        console.log(
          "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
          "\t\tResult: 200 -> Role List Sent"
        );
        return res.status(200).json(data);
      } else {
        console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
        return res.status(403).send({ message: "Forbiden" });
      }
    } else {
      console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
      return res.status(403).send({ message: "Forbiden" });
    }
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const getRoleData = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => GET - Role Data`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2])
      if (authorization) {
    const data = await db.reader("roles", {
      _id: ObjectId(req.params.role_id),
    });
    if (data) {
      console.log(
        "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
        "\t\tResult: 200 -> Role Data Sent"
      );
      return res.status(200).json(data);
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 404 -> Role Not Found"
      );
      return res.status(404).send({ message: "Role Not Found" });;
    }
  } else {
    console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
    return res.status(403).send({ message: "Forbiden" });
  }
} else {
  console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
  return res.status(403).send({ message: "Forbiden" });
}
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const addRole = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => POST - Add Role`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2])
      if (authorization) {
    console.log("REQUEST BODY", req.body);
    let body = req.body;
    const data = await db.creator("roles", body);
    if (data) {
      console.log(
        "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
        "\t\tResult: 200 -> New Role Added"
      );
      return res.status(200).send({ message: "New Role Added" });
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 405 -> Role Not Added"
      );
      return res.status(405).send({ message: "Role Not Added" });
    }
  } else {
    console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
    return res.status(403).send({ message: "Forbiden" });
  }
} else {
  console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
  return res.status(403).send({ message: "Forbiden" });
}
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const modifyRole = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => PUT - Modify Role Data`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2])
      if (authorization) {
    let body = req.body;
    const data = await db.updater(
      "roles",
      {
        _id: ObjectId(req.params.role_id),
      },
      body,
      false
    );
    if (data) {
      console.log(
        "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
        "\t\tResult: 200 -> Role Data Modified"
      );
      return res.status(200);
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 404 -> Role Not Found"
      );
      return res.status(404).send({ message: "Role Not Found" });;
    }
  } else {
    console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
    return res.status(403).send({ message: "Forbiden" });
  }
} else {
  console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
  return res.status(403).send({ message: "Forbiden" });
}
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const deleteRole = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => DELETE - Delete Role`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2])
      if (authorization) {
    console.log("role_id", req.params.role_id);
    const data = await db.deleter("roles", {
      _id: ObjectId(req.params.role_id),
    });
    if (data) {
      console.log(
        "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
        "\t\tResult: 200 -> Role Deleted"
      );
      return res.status(200);
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 404 -> Role Not Found"
      );
      return res.status(404).send({ message: "Role Not Found" });
    }
  } else {
    console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
    return res.status(403).send({ message: "Forbiden" });
  }
} else {
  console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
  return res.status(403).send({ message: "Forbiden" });
}
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

module.exports = {
  getRolesList,
  getRoleData,
  addRole,
  modifyRole,
  deleteRole,
};
