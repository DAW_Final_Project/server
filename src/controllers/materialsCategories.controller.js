const { ObjectId } = require("mongodb");
const { authorizator } = require("../helpers/authorization");
const db = require("../mongodb/db")

const getMaterialsCategoriesList = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => GET - MaterialsCategory List`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2])
      if (authorization) {
        let sort = {};
        if (req.query.sort) {
          let order = req.query.order && req.query.order == "desc" ? -1 : 1;
          sort[req.query.sort] = order;
        }
        let limit = req.query.limit ? parseInt(req.query.limit) : 0;
        let skip = req.query.skip ? parseInt(req.query.skip) : 0;
        const list = await db.reader(
          "materialsCategories",
          {},
          { name: 1, surnames: 1, document: 1 },
          sort,
          skip,
          limit
        );
        const total = await db.counter("materialsCategories");
        const data = {
          list: list,
          total: total,
        };
        console.log(
          "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
          "\t\tResult: 200 -> MaterialsCategory List Sent"
        );
        return res.status(200).json(data);
      } else {
        console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
        return res.status(403).send({ message: "Forbiden" });
      }
    } else {
      console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
      return res.status(403).send({ message: "Forbiden" });
    }
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const getMaterialsCategoryData = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => GET - MaterialsCategory Data`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2])
      if (authorization) {
    const data = await db.reader("materialsCategories", {
      _id: ObjectId(req.params.materialscategory_id),
    });
    if (data) {
      console.log(
        "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
        "\t\tResult: 200 -> MaterialsCategory Data Sent"
      );
      return res.status(200).json(data);
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 404 -> MaterialsCategory Not Found"
      );
      return res.status(404).send({ message: "MaterialsCategory Not Found" });;
    }
  } else {
    console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
    return res.status(403).send({ message: "Forbiden" });
  }
} else {
  console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
  return res.status(403).send({ message: "Forbiden" });
}
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const addMaterialsCategory = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => POST - Add MaterialsCategory`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2])
      if (authorization) {
    console.log("REQUEST BODY", req.body);
    let body = req.body;
    const data = await db.creator("materialsCategories", body);
    if (data) {
      console.log(
        "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
        "\t\tResult: 200 -> New MaterialsCategory Added"
      );
      return res.status(200).send({ message: "New MaterialsCategory Added" });
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 405 -> MaterialsCategory Not Added"
      );
      return res.status(405).send({ message: "MaterialsCategory Not Added" });
    }
  } else {
    console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
    return res.status(403).send({ message: "Forbiden" });
  }
} else {
  console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
  return res.status(403).send({ message: "Forbiden" });
}
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const modifyMaterialsCategory = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => PUT - Modify MaterialsCategory Data`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2])
      if (authorization) {
    let body = req.body;
    const data = await db.updater(
      "materialsCategories",
      {
        _id: ObjectId(req.params.materialscategory_id),
      },
      body,
      false
    );
    if (data) {
      console.log(
        "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
        "\t\tResult: 200 -> MaterialsCategory Data Modified"
      );
      return res.status(200);
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 404 -> MaterialsCategory Not Found"
      );
      return res.status(404).send({ message: "MaterialsCategory Not Found" });;
    }
  } else {
    console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
    return res.status(403).send({ message: "Forbiden" });
  }
} else {
  console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
  return res.status(403).send({ message: "Forbiden" });
}
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

const deleteMaterialsCategory = async (req, res) => {
  console.log(
    "\x1b[35m%s\x1b[0m",
    `(${new Date().toLocaleString("es-ES")}) => DELETE - Delete MaterialsCategory`
  );
  try {
    if (req.headers && req.headers.authorization) {
      let authorization = await authorizator(req.headers.authorization, [1, 2])
      if (authorization) {
    console.log("materialscategory_id", req.params.materialscategory_id);
    const data = await db.deleter("materialsCategories", {
      _id: ObjectId(req.params.materialscategory_id),
    });
    if (data) {
      console.log(
        "\x1b[38;5;28m\x1b[1m%s\x1b[0m",
        "\t\tResult: 200 -> MaterialsCategory Deleted"
      );
      return res.status(200);
    } else {
      console.log(
        "\x1b[38;5;214m\x1b[1m%s\x1b[0m",
        "\t\tResult: 404 -> MaterialsCategory Not Found"
      );
      return res.status(404).send({ message: "MaterialsCategory Not Found" });
    }
  } else {
    console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
    return res.status(403).send({ message: "Forbiden" });
  }
} else {
  console.log('\x1b[38;5;214m\x1b[1m%s\x1b[0m',"\t\tResult: 403 -> Access Denied")
  return res.status(403).send({ message: "Forbiden" });
}
  } catch (error) {
    console.log("\x1b[31m\x1b[1m%s\x1b[0m", "\t\tResult: 500 -> Server Error");
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

module.exports = {
  getMaterialsCategoriesList,
  getMaterialsCategoryData,
  addMaterialsCategory,
  modifyMaterialsCategory,
  deleteMaterialsCategory,
};
