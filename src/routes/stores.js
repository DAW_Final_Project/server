const { Router } = require("express");
const router = Router();

const {
  getStoresListForLogin,
  getStoresList,
  getStoreData,
  addStore,
  modifyStore,
  deleteStore,
} = require("../controllers/stores.controller");

router.get("/listlogin", getStoresListForLogin);

router.get("/list", getStoresList);

router.get("/data/:store_id", getStoreData);

router.post("/add", addStore);

router.put("/modify/:store_id", modifyStore);

router.delete("/delete/:store_id", deleteStore);

module.exports = router;
