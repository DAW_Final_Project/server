const { Router } = require("express");
const router = Router();

const {
  getProductsCategoriesList,
  getProductsCategoryData,
  addProductsCategory,
  modifyProductsCategory,
  deleteProductsCategory,
} = require("../controllers/productscategories.controller");

router.get("/list", getProductsCategoriesList);

router.get("/data/:productsCategory_id", getProductsCategoryData);

router.post("/add", addProductsCategory);

router.put("/modify/:productsCategory_id", modifyProductsCategory);

router.delete("/delete/:productsCategory_id", deleteProductsCategory);

module.exports = router;
