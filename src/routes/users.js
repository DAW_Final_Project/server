const { Router } = require("express");
const router = Router();

const {
  getUsersList,
  getUserData,
  addUser,
  modifyUser,
  deleteUser,
} = require("../controllers/users.controller");

router.get("/list", getUsersList);

router.get("/data/:user_id", getUserData);

router.post("/add", addUser);

router.put("/modify/:user_id", modifyUser);

router.delete("/delete/:user_id", deleteUser);

module.exports = router;
