const { Router } = require("express");
const router = Router();

const { login } = require("../controllers/index.controller");

router.get("/", async (req, res) => {
  res.status(200).send(
    '<label>El servidor funciona</label>'
  );
});

router.get("/login", login);

module.exports = router;
