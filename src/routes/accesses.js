const { Router } = require("express");
const router = Router();

const {
  getAccessList,
  getAccessData,
  addAccess,
  modifyAccess,
  deleteAccess,
} = require("../controllers/accesses.controller");

router.get("/list", getAccessList);

router.get("/data/:access_id", getAccessData);

router.post("/add", addAccess);

router.put("/modify/:access_id", modifyAccess);

router.delete("/delete/:access_id", deleteAccess);

module.exports = router;
