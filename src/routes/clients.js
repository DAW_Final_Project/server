const { Router } = require("express");
const router = Router();

const {
  getClientsList,
  getClientData,
  addClient,
  modifyClient,
  deleteClient,
} = require("../controllers/clients.controller");

router.get("/list", getClientsList);

router.get("/data/:client_id", getClientData);

router.post("/add", addClient);

router.put("/modify/:client_id", modifyClient);

router.delete("/delete/:client_id", deleteClient);

module.exports = router;
