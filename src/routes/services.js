const { Router } = require("express");
const router = Router();

const {
  getServicesListByClient,
  getServiceData,
  addService,
  modifyService,
  deleteService
} = require("../controllers/services.controller");

router.get("/", async (req, res) => {
  res.status(200).send(
    '<label>El servidor funciona</label>'
  );
});

router.get("/list/:client_id", getServicesListByClient);

router.get("/data/:service_id", getServiceData);

router.post("/add", addService);

router.put("/modify/:service_id", modifyService);

router.delete("/delete/:service_id", deleteService);

module.exports = router;
