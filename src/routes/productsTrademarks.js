const { Router } = require("express");
const router = Router();

const {
  getTrademarksList,
  getTrademarkData,
  addTrademark,
  modifyTrademark,
  deleteTrademark,
} = require("../controllers/productstrademarks.controller");

router.get("/list", getTrademarksList);

router.get("/data/:trademark_id", getTrademarkData);

router.post("/add", addTrademark);

router.put("/modify/:trademark_id", modifyTrademark);

router.delete("/delete/:trademark_id", deleteTrademark);

module.exports = router;
