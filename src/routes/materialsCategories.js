const { Router } = require("express");
const router = Router();

const {
  getMaterialsCategoriesList,
  getMaterialsCategoryData,
  addMaterialsCategory,
  modifyMaterialsCategory,
  deleteMaterialsCategory,
} = require("../controllers/materialscategories.controller");

router.get("/list", getMaterialsCategoriesList);

router.get("/data/:materialsCategory_id", getMaterialsCategoryData);

router.post("/add", addMaterialsCategory);

router.put("/modify/:materialsCategory_id", modifyMaterialsCategory);

router.delete("/delete/:materialsCategory_id", deleteMaterialsCategory);

module.exports = router;
