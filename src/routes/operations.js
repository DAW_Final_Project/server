const { Router } = require("express");
const router = Router();

const {
  getOperationsList,
  getOperationData,
  addOperation,
  modifyOperation,
  deleteOperation,
} = require("../controllers/operations.controller");

router.get("/list/:product_id", getOperationsList);

router.get("/data/:operation_id", getOperationData);

router.post("/add", addOperation);

router.put("/modify/:operation_id", modifyOperation);

router.delete("/delete/:operation_id", deleteOperation);

module.exports = router;
