const { Router } = require("express");
const router = Router();

const {
  getStatusList,
  getStatusData,
  addStatus,
  modifyStatus,
  deleteStatus,
} = require("../controllers/status.controller");

router.get("/list", getStatusList);

router.get("/data/:status_id", getStatusData);

router.post("/add", addStatus);

router.put("/modify/:status_id", modifyStatus);

router.delete("/delete/:status_id", deleteStatus);

module.exports = router;
