const { Router } = require("express");
const router = Router();

const {
  getRolesList,
  getRoleData,
  addRole,
  modifyRole,
  deleteRole,
} = require("../controllers/roles.controller");

router.get("/list", getRolesList);

router.get("/data/:role_id", getRoleData);

router.post("/add", addRole);

router.put("/modify/:role_id", modifyRole);

router.delete("/delete/:role_id", deleteRole);

module.exports = router;
