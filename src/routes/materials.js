const { Router } = require("express");
const router = Router();

const {
  getMaterialsList,
  getMaterialData,
  addMaterial,
  modifyMaterial,
  deleteMaterial,
} = require("../controllers/materials.controller");

router.get("/list", getMaterialsList);

router.get("/data/:material_id", getMaterialData);

router.post("/add", addMaterial);

router.put("/modify/:material_id", modifyMaterial);

router.delete("/delete/:material_id", deleteMaterial);

module.exports = router;
