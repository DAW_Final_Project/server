const { Router } = require("express");
const router = Router();

const {
  getProductsList,
  getProductData,
  addProduct,
  modifyProduct,
  deleteProduct,
} = require("../controllers/products.controller");

router.get("/list/:category_id/:trademark_id", getProductsList);

router.get("/data/:product_id", getProductData);

router.post("/add", addProduct);

router.put("/modify/:product_id", modifyProduct);

router.delete("/delete/:product_id", deleteProduct);

module.exports = router;
