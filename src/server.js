// Required packages
const dotenv = require("dotenv").config();
const express = require("express");
const cors = require("cors");
const db = require("./mongodb/db");

// Imported Routes
const indexRoutes = require("./routes/index");
const accessRoutes = require("./routes/accesses");
const clientsRoutes = require("./routes/clients");
const materialsRoutes = require("./routes/materials");
const materialscategoriesRoutes = require("./routes/materialscategories");
const operationsRoutes = require("./routes/operations");
const productsRoutes = require("./routes/products");
const productscategoriesRoutes = require("./routes/productscategories");
const productstrademarksRoutes = require("./routes/productstrademarks");
const rolesRoutes = require("./routes/roles");
const servicesRoutes = require("./routes/services");
const statusRoutes = require("./routes/status");
const storesRoutes = require("./routes/stores");
const usersRoutes = require("./routes/users");
// MAIN FUNCTION
const exec = async () => {
  try {
    // Initialization
    const app = express();

    // Settings
    app.set("port", process.env.SERVER_PORT || 4323);

    // Middlewares
    app.use(express.urlencoded({ extended: false }));
    app.use(express.json());
    const whitelist = [
      "http://modfix.ml",
      "http://modfix.com",
      "http://modfix.es",
      "http://modfix.es:3000",
      "http://localhost",
      "http://localhost:3000",
    ];
    const corsOptions = {
      origin: function (origin, callback) {
        if (whitelist.indexOf(origin) !== -1) {
          callback(null, true);
        } else {
          callback(new Error("Not allowed by CORS"));
        }
      },
    };
    app.use(cors(corsOptions));

    // Routes
    app.use(indexRoutes);
    app.use("/access", accessRoutes);
    app.use("/clients", clientsRoutes);
    app.use("/materials", materialsRoutes);
    app.use("/materialscategories", materialscategoriesRoutes);
    app.use("/operations", operationsRoutes);
    app.use("/products", productsRoutes);
    app.use("/productscategories", productscategoriesRoutes);
    app.use("/productstrademarks", productstrademarksRoutes);
    app.use("/roles", rolesRoutes);
    app.use("/services", servicesRoutes);
    app.use("/status", statusRoutes);
    app.use("/stores", storesRoutes);
    app.use("/users", usersRoutes);
    // Connect Database
    await db.disconnector();
    await db.connector();
    await db.generator();

    // Start Server
    app.listen(app.get("port"));
    console.log("Server on port", app.get("port"));
  } catch (error) {
    console.log(error);
  }
};

exec();
